import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { AjaxService } from '../ajax.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  constructor(
    private ajax: AjaxService
  ) { }
  
  msg;
  cocktail;
  currentCocktails : any;
  @Input() nomeCocktail;
  @Input() opzioneBicchiere;
  @Input() opzioneCategoria;
  @Input() opzioneAlcolica;
  @Input() opzioneIngrediente;

  ngOnInit() {
    this.carica();
  }

  carica() {
    this.msg = "Alcuni dei nostri drink";
    this.loadDrinks("/randomDrinks.json?qta=12");
  }

  getPaginatedItems(item, page, pageSize) {
    let pg = page || 1,
      pgSize = pageSize || 100,
      offset = (pg -1) * pgSize,
      pagedItems = _.drop(item, offset).slice(0, pgSize);
    return pagedItems;
  }

  loadDrinks(url) {
    this.ajax.get(url).subscribe((success) => {
      this.cocktail = success;
      this.currentCocktails = this.getPaginatedItems(this.cocktail, 0, 12);
    });
  }

  paginate(event) {
    this.currentCocktails = this.getPaginatedItems(this.cocktail, event.pageIndex+1, event.pageSize);
  }

  ngOnChanges(changes: SimpleChanges) {
    if( changes['nomeCocktail'] && changes['nomeCocktail'].previousValue != changes['nomeCocktail'].currentValue ) {
      if (this.nomeCocktail) {
        this.nomeCocktail = this.nomeCocktail.trim().toLowerCase();
        if (this.nomeCocktail.length == 1) {
          this.msg = "Drink che hanno come prima lettera: "+this.nomeCocktail.charAt(0);
          
          this.loadDrinks("/drinksPrimaLettera.json?singola_lettera=" + this.nomeCocktail.charAt(0));
        } else {
          this.msg = "Drink che contengono la parola: "+this.nomeCocktail;

          this.loadDrinks("/drinksSearch.json?nome=" + this.nomeCocktail);
        }
      }
    }
    if( changes['opzioneBicchiere'] && changes['opzioneBicchiere'].previousValue != changes['opzioneBicchiere'].currentValue ) {
      if(changes['opzioneBicchiere'].currentValue){        
        this.msg = "Drink si servono in questo bicchiere: "+this.opzioneBicchiere;

        this.loadDrinks("/drinksGlass.json?bicchiere="+this.opzioneBicchiere);
      }
    }
    if( changes['opzioneCategoria'] && changes['opzioneCategoria'].previousValue != changes['opzioneCategoria'].currentValue ) {      
      if(changes['opzioneCategoria'].currentValue){
        this.msg = "Drink che appartengono a questa categoria: "+this.opzioneCategoria;

        this.loadDrinks("/drinksCategoria.json?categoria="+this.opzioneCategoria);
      }
    }
    if( changes['opzioneAlcolica'] && changes['opzioneAlcolica'].previousValue != changes['opzioneAlcolica'].currentValue ) {
      if(changes['opzioneAlcolica'].currentValue){
        this.msg = this.opzioneAlcolica;

        this.loadDrinks("/drinksTipo.json?tipo="+this.opzioneAlcolica);
      }
    }
    if( changes['opzioneIngrediente'] && changes['opzioneIngrediente'].previousValue != changes['opzioneIngrediente'].currentValue ) {
      if(this.opzioneIngrediente){
        this.msg = "Drink con questi ingredienti: "+this.opzioneIngrediente;

        this.loadDrinks("/drinksIngredienti.json?ingredienti="+this.opzioneIngrediente);
      }
    }
  }
}
