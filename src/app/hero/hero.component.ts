import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { AjaxService } from '../ajax.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { MatOption } from '@angular/material/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

  dropdownFiltro = false;
  @Output() nomeCocktail = new EventEmitter();
  ricerca:string;
  bicchieri;
  categorie;
  opzioniAlcoliche;
  ingredienti;
  ingredientiSelezionati: string[];
  randomDrink;

  @Output() opzioneBicchiere = new EventEmitter();
  @Output() opzioneCategoria = new EventEmitter();
  @Output() opzioneAlcolica = new EventEmitter();
  @Output() opzioneIngrediente = new EventEmitter();

  constructor(
    private ajax: AjaxService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.ajax.get("/randomDrink.json?time_zone=" + Intl.DateTimeFormat().resolvedOptions().timeZone).subscribe((success) => {
      this.randomDrink = success;
    });

    this.ajax.get("/listaGlass.json").subscribe((success) => {
      this.bicchieri = success;
    });
    this.ajax.get("/listaCategorie.json").subscribe((success) => {
      this.categorie = success;
    });
    this.ajax.get("/listaTipologie.json").subscribe((success) => {
      this.opzioniAlcoliche = success;
    });
    this.ajax.get("/listaIngredienti.json").subscribe((success) => {
      this.ingredienti = success;
      this.ingredienti.sort();
    });
  }

  openDialog() {
    const dialog = this.dialog.open(DialogComponent, {
      width: "50vw",
      data: { drink: this.randomDrink }
    });
  }

  search() {
    if ( !this.ricerca && this.ingredientiSelezionati && this.ingredientiSelezionati.length > 0) {
      this.opzioneIngredienti(this.ingredientiSelezionati)
    } else {
      this.removeIngredienti();
      this.nomeCocktail.emit(this.ricerca);
    }
  }

  opzioneBicchieri(item) {
    this.opzioneBicchiere.emit(item);
  }

  opzioneCategorie(item) {
    this.opzioneCategoria.emit(item);
  }

  opzioneAlcoliche(item) {
    this.opzioneAlcolica.emit(item);
  }

  opzioneIngredienti(item) {
    this.opzioneIngrediente.emit(item);
    this.removeIngredienti();
    this.ricerca = undefined;
  }

  removeIngrediente(ingrediente) {
    let index = this.ingredientiSelezionati.indexOf(ingrediente);

    if (index >= 0) {
      this.ingredientiSelezionati.splice(index, 1);
      index = this.ingredienti.indexOf(ingrediente);
      this.ingredienti.splice(index,1,ingrediente+" "); // HACKERATA
      this.ingredienti.sort();
    }
  }

  removeIngredienti() {
    this.ingredientiSelezionati = [];
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
}
