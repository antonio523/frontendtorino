import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  constructor(
    private http: HttpClient
  ) {  }

  test(){
    console.log("Ajax correttamente chiamato")
  }

  baseUrl = "http://localhost:8080/cocktail";

  get(path){
    return this.http.get(this.baseUrl + path);
  }

}
