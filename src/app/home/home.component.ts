import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  nomeCocktail: string;
  opzioneBicchiere;
  opzioneCategoria;
  opzioneAlcolica;
  opzioneIngrediente: string[];

  constructor() { }

  ngOnInit(): void {
  }

  setNomeCocktail(nome){
    this.nomeCocktail=nome;
  }

  setOpzioneBicchiere(nome){
    this.opzioneBicchiere=nome;
  }

  setOpzioneCategoria(nome){
    this.opzioneCategoria=nome;
  }

  setOpzioneAlcolica(nome){
    this.opzioneAlcolica=nome;
  }

  setOpzioneIngrediente(nome){
    this.opzioneIngrediente=nome;
  }
}
