import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../ajax.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

drink;
ingredienti;
constructor(
  private route: ActivatedRoute,
  private ajax: AjaxService
) {}
getSpecificoDrink(id) {
  this.ajax.get("/drink.json?id="+id).subscribe((success) => {

    this.drink = success;
    if(this.drink)
      this.getIngredienti(this.drink.ingredienti);
  });
}

getIngredienti(ingredienti) {
  this.ajax.get("/infoIngrediente.json?ingredienti="+ingredienti).subscribe((success) => {
    this.ingredienti = success;
  });
}





  ngOnInit(): void {
   this.route.params.subscribe((result)=>{
     const idDrink=result.nome;     
     this.getSpecificoDrink(idDrink);  
    });
  }

}
